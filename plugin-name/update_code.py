import json
import os
current_path = os.getcwd()
dir_path = os.path.dirname(os.path.realpath(__file__))
# print(current_path)
# print(dir_path)
os.chdir(dir_path)
with open('plugins.json') as json_file:
    data = json.load(json_file)
    list_plugins = data['list']
    # print(list_plugins)
    # cd to parent
    os.chdir('../')
    count = 0
    for folder in list_plugins:
        count +=1
        item = list_plugins[folder]
        path = './'+folder
        if (os.path.exists(path) is False):
            # create folder and clone
            command =  'git clone --branch {} {} {}'.format(item['branch'], item['url'], folder)
        else:
            command = 'git -C {} pull  origin {}'.format(folder,item['branch'])
            # just update
        print '\n{}. Folder:{} => Running {}\n'.format(count, folder, command)
        os.system(command)

os.chdir(current_path)